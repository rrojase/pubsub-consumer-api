class CartHistory {
  constructor (messageId, cartId, cart) {
    this.message_id = messageId;
    this.cart_id = cartId;
    this.cart = JSON.stringify(cart);
  }
}

module.exports = CartHistory;

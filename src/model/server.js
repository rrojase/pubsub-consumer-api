const express = require('express');
const cors = require('cors');

const config = require('../config/environment.config');
const db = require('../config/db.config');

class Server {
  constructor () {
    this.app = express();
    this.port = config.PORT;
    this.appname = config.APP_NAME;
    this.env = config.ENV;
    this.baseUrl = 'api-pubsub-consumer';

    // Database
    this.database();

    // Middlewares
    this.middlewares();

    // Rutas
    this.routes();
  }

  database () {
    db.startConnection();
  }

  middlewares () {
    this.app.use(cors());
    this.app.use(express.json());
  }

  routes () {
    this.app.use(`/${this.baseUrl}/`, require('../router/health.router'));
    this.app.use(`/${this.baseUrl}/`, require('../router/cartHistory.router'));
  }

  start () {
    this.app.listen(this.port, () => {
      console.log(`* Starting ${this.appname} on port: ${this.port}\n * Running in: ${this.env}`);
    });
  }
}

module.exports = Server;

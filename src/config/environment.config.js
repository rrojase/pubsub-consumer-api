const pf = require(`./profile/${process.env.NODE_ENV || 'local'}.profile`);

const config = {
  APP_NAME: process.env.APP_NAME ,
  PORT: process.env.PORT,
  ENV: process.env.NODE_ENV,
  dbCredentials: {
    host: pf.db_host,
    user: pf.db_user,
    pass: pf.db_pass,
    db: pf.db_name,
    port: pf.db_port
  },
}

module.exports = config;

require('dotenv').config();

module.exports = {
  db_host: process.env.PG_DB_HOST,
  db_user: process.env.PG_DB_USER,
  db_pass: process.env.PG_DB_PASS,
  db_name: process.env.PG_DB_BASE,
  db_port: process.env.PG_DB_PORT
}

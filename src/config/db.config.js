const { Pool } = require('pg');

const log = require('../common/logger.common').getLogger();
const { dbCredentials } = require('./environment.config');

const getPool = () => {
  try {
    return new Pool({
      user: dbCredentials.user,
      password: dbCredentials.pass,
      host: dbCredentials.host,
      port: dbCredentials.port,
      database: dbCredentials.db
    })
  } catch (err) {
    log.error(err);
  }
}

var dbConnInstance = getPool();

const startConnection = () => {
  try {
    return dbConnInstance.connect((err, client, done) => {
      if (err) {
        log.error(`Postgres: Error to connecting ${err}`);
        return done(err);
      }
      log.info(`Postgres: Database connected`);
      return client;
  });
  } catch (err) {
    log.error(err);
  }
}

const query = (sql, values, operation) => {
  let arrValues = [];
  if (operation === 'insert') {
    Object.keys(values).map(key => {
      arrValues.push(values[key]);
    });
  } else {
    arrValues = values;
  }
  
  return new Promise((resolve, reject) => {
    dbConnInstance.query(sql, arrValues, (err, res) => {
      if (err) {
        reject(err);
      } else {
        resolve(res);
      }
      // close connection
    });
  });
}

module.exports = {
  startConnection,
  query
}

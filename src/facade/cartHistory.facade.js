const { query } = require('../config/db.config');
const log = require('../common/logger.common').getLogger();
const CartHistory = require('../model/CartHistory');
const tableName = 'carts_history';

const storeCartHistory = async cartHistory => {
  try {
    const sql = `INSERT INTO ${tableName} (message_id, cart_id, cart) VALUES($1, $2, $3);`;
    const res = await query(sql, cartHistory, 'insert');
    log.info(`res: ${JSON.stringify(res)}`)
    return res;
  } catch (err) {
    log.error(err);
  }
}

module.exports = {
  storeCartHistory
};

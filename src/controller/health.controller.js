const os = require('os');

const package = require('../../package.json');

const checkHealth = (req, res) => {
  const data = {
    app: {
      version: package.version,
      build: process.env.APP_BUILD || 'no-build',
      commit: process.env.APP_COMMIT || 'no-commit',
      name: package.name,
      environment: process.env.NODE_ENV,
    },
    hostname: os.hostname(),
  };

  return res.status(200).json(data);
}

module.exports = {
  checkHealth
};

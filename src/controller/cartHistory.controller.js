const log = require('../common/logger.common').getLogger();

const cartHistoryService = require('../services/cartHistory.service');

const storeCartHistory = async (req, res) => {
  const { body } = req;
  try {
    const response = await cartHistoryService.storeCartHistory(body);
    return res.status(200).json({
      status: 200,
      failed: false,
      data: response.data
    });
  } catch (err) {
    log.error(err);
    return res.status(500).json({
      status: 500,
      failed: true,
      error: JSON.stringify(err)
    });
  }
};

module.exports = { storeCartHistory };

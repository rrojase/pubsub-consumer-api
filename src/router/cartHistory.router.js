const { Router } = require('express');
const { storeCartHistory } = require('../controller/cartHistory.controller');

const router = Router();

router.post('/cart-history', storeCartHistory);

module.exports = router;
const { Router } = require('express');
const { checkHealth } = require('../controller/health.controller');

const router = Router();

router.get('/health', checkHealth);

module.exports = router;

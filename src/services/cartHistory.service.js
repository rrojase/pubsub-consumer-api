const cartHistoryFacade = require("../facade/cartHistory.facade");

const storeCartHistory = async (data) => {
  return await cartHistoryFacade.storeCartHistory(data);
}

module.exports = { storeCartHistory };

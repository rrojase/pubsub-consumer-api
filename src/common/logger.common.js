const logger = require('@cocha/cocha-logger');
const { APP_NAME } = require('../config/environment.config');

var loggerInstance;

const getLogger = () => {
  if (!loggerInstance) {
    loggerInstance = logger.getLogger({ appName: APP_NAME });
  }
  return loggerInstance;
}

module.exports = { getLogger };
